require('dotenv').config()
const express = require('express')
const jwt = require('jsonwebtoken');
const cors = require('cors')
const mongoose = require('mongoose');
const Product = require('./product')
const bcrypt = require('bcrypt');
const saltRounds = 10;
const app = express()
const port = process.env.PORT || 3000;




app.use(cors())
app.use(express.json())





const userSchema = new mongoose.Schema({
    name: String,
    email:String,
    password:String
  });

  const User = mongoose.model('User', userSchema);

  

app.post('/users/signup', async(req,res) =>{
    const hash = bcrypt.hashSync(req.body.password, saltRounds);
    const user = new User({
        name: req.body.name,
        email: req.body.email,
        password: hash
    })
    await user.save()
    res.status(201).json(user)
})

app.post('/users/login', async(req,res) => {
  const {email,password} = req.body
  try{
    const user = await User.findOne({ email: email }).exec();
    if(!user){
      return res.status(404).send('Email not found')
    }
    const passwordmatch = bcrypt.compareSync(password, user.password);
    if(!passwordmatch) {
      return res.status(401).send('Invalid password')
    }
    const token = jwt.sign({ id: user._id, name: user.name }, process.env.TOKEN_SECRET);
    res.json(token)
  }
  catch(error){
    console.log(error)
  }
})

app.get('/products', async (req,res) => {
  const products = await Product.find({});
  res.status(200).json(products)
})

app.post('/products', async (req,res) => {
  const product = new Product(req.body)
  await product.save()
  res.status(201).json(product)
})

app.get('/', (req, res) => {
  res.send('Hello, World!');
});

app.listen(port, () => {
  console.log(`Example app listening on port ${port}`)
})



main().then(console.log('Db Connected')).catch(err => console.log(err));

async function main() {
  const url = process.env.DB_URL;
  const password = process.env.DB_PASSWORD;

  // Debugging information
  console.log('DB_URL:', url);
  console.log('DB_PASSWORD:', password);

  // Check if either DB_URL or DB_PASSWORD is not defined
  if (!url || !password) {
      console.error('DB_URL or DB_PASSWORD is not defined.');
      return;
  }

  // Replace <password> placeholder in the URL
  const urlwithpassword = url.replace('<password>', password);

  // Debugging information
  console.log('URL with Password:', urlwithpassword);

  // Connect to MongoDB
  try {
      await mongoose.connect(urlwithpassword);
      console.log('Connected to MongoDB');
  } catch (error) {
      console.error('Error connecting to MongoDB:', error.message);
  }
}

